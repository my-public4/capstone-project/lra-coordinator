FROM gcr.io/distroless/java17-debian11
WORKDIR /project
COPY target target
ENTRYPOINT ["java", "-jar", "target/quarkus-app/quarkus-run.jar", "-Dquarkus.http.port=8080"]